package com.example.joao.aula10_ifswitch;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText campoNumero;
    private EditText campoSenha;

    private TextView textoNumero;
    private TextView textoSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        campoNumero = (EditText)findViewById(R.id.campoNumero);
        campoSenha = (EditText)findViewById(R.id.campoSenha);

        textoNumero = (TextView)findViewById(R.id.textoNumero);
        textoSenha = (TextView)findViewById(R.id.textoSenha);


    }

    public void clickNumero(View v){
        String valor = campoNumero.getText().toString();
        int num = Integer.parseInt(valor);

        if (num%2==0){
            textoNumero.setText("O número é par!");
        }
        else {
            textoNumero.setText("O número é impar!");
        }
    }

    public void clickSenha(View v){
        String senha = campoSenha.getText().toString();

        switch(senha){
            case "felpudo":
                textoSenha.setText("Senha correta!");
                break;
            case "FELPUDO":
                textoSenha.setText("Senha correta!");
                break;
            case "felpudinho":
                textoSenha.setText("Senha correta!");
                break;
            default:
                textoSenha.setText("Senha incorreta");
                break;
        }
    }


}
