package com.example.joao.aula12_forwhile;

import android.graphics.Color;
import android.support.annotation.StringDef;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView textoResultado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textoResultado = (TextView)findViewById(R.id.textoResultado);
        textoResultado.setText(" ");
    }

    public void clicaFor(View v){
        textoResultado.setText(" ");
        textoResultado.setTextColor(Color.GREEN);
        for (int i = 0; i < 10; i++){
            String stringint = Integer.toString(i);
            textoResultado.append("For 0 a " + stringint + "\n");
            textoResultado.refreshDrawableState();
        }
    }

    public void clicaLimpar(View v){
        textoResultado.setText(" ");
    }

    public void clicaWhile(View v){
        textoResultado.setText(" ");
        textoResultado.setTextColor(Color.YELLOW);

        int j = 0;

        while(j<10){
            String stringint = Integer.toString(j);
            textoResultado.append("Nome posição " + stringint + "\n");
            j++;
        }
    }
}
