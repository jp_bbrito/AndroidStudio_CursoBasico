package com.example.joao.aula6_imagebutton;

import android.support.annotation.StringDef;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void alertaNormal(View V){
        AlertDialog alerta;
        alerta = new AlertDialog.Builder(this).create();
        alerta.setTitle("Funcionou!");
        alerta.setMessage("Botão Alerta 1");
        alerta.show();
    }
    public void alertaX(View V){
        Button button = (Button) V;

        String mensagem = "Você apertou o ";

        mensagem = mensagem.concat(button.getText().toString());

        AlertDialog  alerta = new AlertDialog.Builder(this).create();
        alerta.setTitle("Botão Apertado");
        alerta.setMessage(mensagem);
        alerta.show();
    }

}
